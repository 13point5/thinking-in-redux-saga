import { nanoid } from "nanoid";

export const fields = [
  {
    id: nanoid(),
    type: "text",
    label: "Cause of the bug",
    dependentFields: [],
  },
  {
    id: nanoid(),
    type: "date",
    label: "Due Date",
    dependentFields: [],
  },
  {
    id: nanoid(),
    type: "text",
    label: "Assignee",
    dependentFields: [],
  },
  {
    id: nanoid(),
    type: "text",
    label: "Feature",
    dependentFields: [],
  },
  {
    id: nanoid(),
    type: "text",
    label: "Feature task",
    dependentFields: [],
  },
  {
    id: nanoid(),
    type: "section",
    label: "Random section",
    fields: [
      {
        id: nanoid(),
        type: "link",
        label: "Feature",
        dependentFields: [],
      },
      {
        id: nanoid(),
        type: "link",
        label: "Demo",
        dependentFields: [],
      },
      {
        id: nanoid(),
        type: "text",
        label: "Assignee",
        dependentFields: [],
      },
      {
        id: nanoid(),
        type: "text",
        label: "Feature",
        dependentFields: [],
      },
      {
        id: nanoid(),
        type: "text",
        label: "Feature task",
        dependentFields: [],
      },
    ],
  },
  {
    id: nanoid(),
    type: "section",
    label: "Second section",
    fields: [
      {
        id: nanoid(),
        type: "text",
        label: "Bla",
        dependentFields: [],
      },
      {
        id: nanoid(),
        type: "text",
        label: "Magic",
        dependentFields: [],
      },
    ],
  },
];
