import { fields } from "./fakeData";

export const fetchFields = () =>
  new Promise((resolve) => setTimeout(() => resolve(fields), 1500));

export const fetchFieldValue = ({ signal }) =>
  fetch("http://metaphorpsum.com/sentences/1", {
    signal,
  }).then((res) => res.text());
