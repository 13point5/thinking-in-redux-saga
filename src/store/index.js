import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";

import sagas from "../sagas";
import { reducer as checklist } from "./checklist";

const sagaMiddleware = createSagaMiddleware();

const store = configureStore({
  reducer: {
    checklist,
  },
  middleware: [...getDefaultMiddleware({ thunk: false }), sagaMiddleware],
});

sagaMiddleware.run(sagas);

export default store;
