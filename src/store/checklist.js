import { createSlice, createAction } from "@reduxjs/toolkit";

const initialState = {
  open: false,
  fields: {
    fetching: false,
    error: null,
    topLevel: [],
    sections: {},
    values: {},
  },
};

const checklistSlice = createSlice({
  name: "checklist",
  initialState,
  reducers: {
    open: (state) => {
      state.open = true;
      state.fields.fetching = true;
    },
    close: (state) => initialState,

    fetchFieldsSuccess: (state, { payload }) => {
      let now = Date.now();
      const after = now + 1000 * 2;
      while (Date.now() <= after) {
        now = now + 1;
      }

      state.fields.fetching = false;

      payload.forEach((item) => {
        if (item.type !== "section") {
          state.fields.topLevel.push(item);
          state.fields.values[item.id] = {
            fetching: true,
            data: null,
            error: null,
          };
        } else {
          state.fields.sections[item.id] = {
            ...item,
            open: false,
          };
        }
      });
    },
    fetchFieldsError: (state, { payload }) => {
      state.fields.error = payload;
      state.fields.fetching = false;
    },

    fetchFieldValueRequest: (state, { payload: id }) => {
      state.fields.values[id] = {
        fetching: true,
        data: null,
        error: null,
      };
    },
    fetchFieldValueSuccess: (state, { payload: { id, data } }) => {
      state.fields.values[id] = {
        fetching: false,
        data,
        error: null,
      };
    },
    fetchFieldValueError: (state, { payload: { id, error } }) => {
      state.fields.values[id] = {
        fetching: false,
        data: null,
        error,
      };
    },

    openSection: (state, { payload: id }) => {
      if (!state.fields.sections[id]) {
        throw new Error("Section not found!");
      }

      state.fields.sections[id].open = true;
    },
    closeSection: (state, { payload: id }) => {
      if (!state.fields.sections[id]) {
        throw new Error("Section not found!");
      }

      state.fields.sections[id].open = false;

      const sectionFields = state.fields.sections[id].fields;

      sectionFields.forEach((field) => {
        // If the field value is being fetched then reset it
        if (state.fields.values[field.id].fetching) {
          state.fields.values[field.id] = null;
        }
      });
    },
  },
});

export const reducer = checklistSlice.reducer;

const fetchTopLevelFieldValues = createAction(
  "checklist/fetchTopLevelFieldValues"
);

export const actions = { ...checklistSlice.actions, fetchTopLevelFieldValues };
