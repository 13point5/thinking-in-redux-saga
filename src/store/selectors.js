export const topLevelFieldIds = (state) =>
  state.checklist.fields.topLevel.map((field) => field.id);

export const sectionFieldIds = (sectionId) => (state) =>
  state.checklist.fields.sections[sectionId].fields.map((field) => field.id);

export const sectionFieldsToFetch = (sectionId) => (state) => {
  const section = state.checklist.fields.sections[sectionId];

  const values = state.checklist.fields.values;

  const fieldsToFetch = section.fields.filter((field) => !values[field.id]);

  return fieldsToFetch;
};

export const fieldValue = (id) => (state) => state.checklist.fields.values[id];
