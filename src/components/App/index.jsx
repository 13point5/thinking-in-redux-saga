import { useSelector, useDispatch } from "react-redux";

import { Grid, Button } from "@mui/material";

import { actions as checklistActions } from "store/checklist";

import Checklist from "components/Checklist";
import * as styles from "./styles";

function App() {
  const dispatch = useDispatch();
  const isChecklistOpen = useSelector((state) => state.checklist.open);

  const openChecklist = () => dispatch(checklistActions.open());

  return (
    <Grid container sx={styles.container}>
      <Grid item xs={9.5} sx={styles.chatroom}>
        <Button variant="contained" onClick={openChecklist}>
          open checklist
        </Button>
      </Grid>
      <Grid item xs={2.5} hidden={!isChecklistOpen}>
        <Checklist />
      </Grid>
    </Grid>
  );
}

export default App;
