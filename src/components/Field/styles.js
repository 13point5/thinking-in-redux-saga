export const container = {
  px: 1.5,
  py: 1,
};

export const content = {
  gap: 1.5,
};

export const header = {
  flexDirection: "row",
  alignItems: "center",
  gap: 1,
};

export const loader = {
  m: "auto",
};
