import { useSelector } from "react-redux";

import {
  Alert,
  Card,
  Typography,
  Stack,
  CircularProgress,
} from "@mui/material";
import { TextSnippet } from "@mui/icons-material";

import * as selectors from "store/selectors";

import * as styles from "./styles";

export default function Field({ type, label, id }) {
  const value = useSelector(selectors.fieldValue(id));

  return (
    <Card variant="outlined" sx={styles.container}>
      <Stack sx={styles.content}>
        <Stack sx={styles.header}>
          <TextSnippet color="primary" />

          <Typography>{label}</Typography>
        </Stack>

        {value.fetching && <CircularProgress size={20} sx={styles.loader} />}

        {value.error && <Alert severity="error">{value.error}</Alert>}

        {value.data !== null && <Typography>{value.data}</Typography>}
      </Stack>
    </Card>
  );
}
