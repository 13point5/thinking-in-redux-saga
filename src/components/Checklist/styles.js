export const container = {
  height: "100%",
  borderLeft: "1px solid #ccc",
};

export const header = {
  flexDirection: "row",
  alignItems: "center",
  justifyContent: "space-between",
  py: 1,
  px: 2,
  borderBottom: "1px solid #ccc",
};

export const content = {
  p: 2,
};

export const loader = {
  m: "auto",
};
