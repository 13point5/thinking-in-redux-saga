import { useDispatch, useSelector } from "react-redux";

import {
  Stack,
  Typography,
  IconButton,
  CircularProgress,
  Alert,
} from "@mui/material";
import { Close as CloseIcon } from "@mui/icons-material";

import { actions as checklistActions } from "store/checklist";

import Field from "components/Field";
import Section from "components/Section";
import * as styles from "./styles";

export default function Checklist() {
  const dispatch = useDispatch();

  const { fields: fieldsState } = useSelector((state) => state.checklist);
  // console.log(`fieldsState`, fieldsState);

  const closeChecklist = () => dispatch(checklistActions.close());

  return (
    <Stack sx={styles.container}>
      <Stack sx={styles.header}>
        <Typography>Checklist</Typography>

        <IconButton onClick={closeChecklist}>
          <CloseIcon />
        </IconButton>
      </Stack>

      <Stack sx={styles.content} spacing={2}>
        {fieldsState.fetching && <CircularProgress sx={styles.loader} />}

        {fieldsState.error && (
          <Alert severity="error">{fieldsState.error}</Alert>
        )}

        {fieldsState.topLevel.map((field) => (
          <Field key={field.id} {...field} />
        ))}

        {Object.values(fieldsState.sections).map((section) => (
          <Section key={section.id} {...section} />
        ))}
      </Stack>
    </Stack>
  );
}
