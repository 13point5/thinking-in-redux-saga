import { useDispatch } from "react-redux";

import {
  Card,
  ListItemButton,
  ListItemText,
  ListItemIcon,
  Collapse,
  Stack,
} from "@mui/material";
import { AcUnit, ExpandLess, ExpandMore } from "@mui/icons-material";

import { actions as checklistActions } from "store/checklist";

import Field from "components/Field";
import * as styles from "./styles";

export default function Section({ id, open, label, fields }) {
  const dispatch = useDispatch();

  const handleToggleCollapse = () => {
    if (open) {
      dispatch(checklistActions.closeSection(id));
    } else {
      dispatch(checklistActions.openSection(id));
    }
  };

  return (
    <Card variant="outlined">
      <ListItemButton onClick={handleToggleCollapse}>
        <ListItemIcon>
          <AcUnit />
        </ListItemIcon>
        <ListItemText primary={label} />
        {open ? <ExpandLess /> : <ExpandMore />}
      </ListItemButton>

      <Collapse in={open}>
        {open && (
          <Stack spacing={2} sx={styles.fieldsContainer}>
            {fields.map((field) => (
              <Field key={field.id} {...field} />
            ))}
          </Stack>
        )}
      </Collapse>
    </Card>
  );
}
