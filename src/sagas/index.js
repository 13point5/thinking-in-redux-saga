import { all } from "redux-saga/effects";

import checklistSagas from "./checklist";

function* rootSaga() {
  yield all([...checklistSagas]);
}

export default rootSaga;
