import {
  all,
  put,
  call,
  takeLatest,
  select,
  take,
  takeEvery,
  race,
  cancelled,
  takeLeading,
} from "redux-saga/effects";
import { channel } from "redux-saga";

import * as checklistAPI from "api/checklist";
import { actions as checklistActions } from "store/checklist";
import * as selectors from "store/selectors";

function* fetchChecklist() {
  try {
    const fields = yield call(checklistAPI.fetchFields);
    yield put(checklistActions.fetchFieldsSuccess(fields));
    yield put(checklistActions.fetchTopLevelFieldValues());
  } catch (error) {
    yield put(checklistActions.fetchFieldsError(error.message));
  }
}

function* watchFetchChecklist() {
  yield takeLatest(checklistActions.open.type, fetchChecklist);
}

function* getChecklistFieldValue(fieldId) {
  const state = yield select();
  if (!state.checklist.fields.values[fieldId]) return;

  const abortController = new AbortController();

  try {
    const fieldValue = yield call(checklistAPI.fetchFieldValue, {
      signal: abortController.signal,
    });

    yield put(
      checklistActions.fetchFieldValueSuccess({
        id: fieldId,
        data: fieldValue,
      })
    );
  } catch (error) {
    yield put(
      checklistActions.fetchFieldValueError({
        id: fieldId,
        error: error.message,
      })
    );
  } finally {
    if (yield cancelled()) {
      abortController.abort();
    }
  }
}

function* watchSectionClose(fieldId) {
  while (true) {
    const { payload: sectionId } = yield take(
      checklistActions.closeSection.type
    );

    const sectionFieldIds = yield select(selectors.sectionFieldIds(sectionId));

    if (sectionFieldIds.includes(fieldId)) break;
  }
}

function* handleFetchFieldValue(fieldId) {
  yield race([
    call(getChecklistFieldValue, fieldId),
    call(watchSectionClose, fieldId),
    take(checklistActions.close.type),
  ]);
}

const N_FIELDS_AT_A_TIME = 3;

function* watchFetchFieldValues() {
  const addTaskChannel = yield call(channel);

  yield all(
    Array(N_FIELDS_AT_A_TIME).fill(
      takeLeading(addTaskChannel, handleFetchFieldValue)
    )
  );

  yield takeLatest(
    checklistActions.fetchTopLevelFieldValues.type,
    function* () {
      const fieldsToFetch = yield select(selectors.topLevelFieldIds);

      yield all(fieldsToFetch.map((fieldId) => put(addTaskChannel, fieldId)));
    }
  );

  yield takeEvery(checklistActions.openSection.type, function* ({ payload }) {
    const fieldsToFetch = yield select(selectors.sectionFieldsToFetch(payload));

    yield all(
      fieldsToFetch.map((field) =>
        put(checklistActions.fetchFieldValueRequest(field.id))
      )
    );

    yield all(fieldsToFetch.map((field) => put(addTaskChannel, field.id)));
  });
}

const sagas = [watchFetchChecklist(), watchFetchFieldValues()];

export default sagas;
